const express = require('express');
const routes = express.Router();


const userController = require('../controllers/userController');

routes.get('/users', userController.user);
routes.post('/users', userController.insert);

module.exports = routes;