const knex = require('../database');

module.exports = {
    async user(req, res) { 
        const results = await knex('usuarios');

        return res.json(results).status(200);
    },
    async insert(req, res, next) {
        try {
            const { username } = req.body;
            await knex('usuarios').insert({
            username
        });
        return res.status(201).send();
        } catch (error) {
            next(error)
        }
    }
}